let burger = document.getElementById('burger');
let burgerMenu = document.querySelector('.header__mobile-menu')
burger.addEventListener('click', (element) => {
    burger.classList.toggle('Diam');
    burgerMenu.classList.toggle('active-mobileMenu')
})


const animItems = document.querySelectorAll('.animItems');

if (animItems.length > 0) {
    window.addEventListener('scroll', animFromScroll);
    function animFromScroll() {
        for (let index = 0; index < animItems.length; index++) {
            const animItem = animItems[index];
            const animItemHeight = animItem.offsetHeight;
            const animItemOffset = offset(animItem).top;
            const animStart = 4;

            let animItemPoint = window.innerHeight - animItemHeight / animStart;
            if (animItemHeight > window.innerHeight) {
                animItemPoint = window.innerHeight - window.innerHeight / animStart;
            }

            if ((pageYOffset > animItemOffset - animItemPoint) && pageYOffset < (animItemOffset + animItemHeight)) {
                animItem.classList.add('active');
            } else {
                animItem.classList.remove('active')
            }


        }
    }
    function offset(el) {
        const rect = el.getBoundingClientRect(),
            scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
            scrollTop = window.pageYOffset || document.documentElement.scrollTop;
        return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
    }
    setTimeout(() => {
        animFromScroll()
    }, 1000);

}


const swipeContainer = document.querySelector('.swiper-container');
const list = document.querySelector('.swiper-list');
const wrapper = document.querySelector('.swiper-wrapper');
const slides = document.querySelectorAll('.swiper-slide');
const left = document.querySelector('.guests__left');
const right = document.querySelector('.guests__right');
let translate = val => `translate(${val})`; 
    let rem = 'rem';
    let count = 0;
right.addEventListener('click', () => {
 if (count === -76) {
    return count = -76;
 }
 count -= 38;
 for (let elem of slides) {
    elem.style.transform = translate( count + rem ); 
 }
})
left.addEventListener('click', () => {
    if (count === 0) {
        return count = 0;
    }
    count += 38;
    for (let elem of slides) {
       elem.style.transform = translate( count + rem ); 
    }

   })



const comentators = document.querySelectorAll('.link-mobile');
const coments = document.querySelectorAll('.coment-mobile');
let  activeBtn = document.querySelector('.activebtn');
for (let elem of coments) {
    if (elem.dataset.name === activeBtn.dataset.name) {
        elem.style.display = 'block';
    } else if (elem.dataset.name !== activeBtn.dataset.name) {
        elem.style.display = 'none';
    }
   }
[...comentators].forEach((item) => {
    item.addEventListener('click', (event) => {
        for (let elem of comentators) {
        elem.classList.remove('activebtn');
       }
       event.target.classList.add('activebtn');
       for (let elem of coments) {
        if (elem.dataset.name === event.target.dataset.name) {
            elem.style.display = 'block';
        } else if (elem.dataset.name !== event.target.dataset.name) {
            elem.style.display = 'none';
        }
       }
    })
})





